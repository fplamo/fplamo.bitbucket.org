<html>
<body style="max-width: 40em;">

<h1>CanvasScript</h1>
<p><strong>CanvasScript</strong> is a command-based language to draw graphics. The feature set of the language is heavily inspired by the JavaScript API for the HTML <code>&lt;canvas&gt;</code> element.

<p>You paint the canvas by writing a list of commands. You can draw rectangles with <code>strokeRect</code> and <code>fillRect</code>. You can draw text with <code>fillText</code> and <code>strokeText</code>. You can draw paths with <code>fill</code> and <code>stroke</code>. You can change drawing attributes with <code>set</code>. You can apply linear transformations with <code>translate</code>, <code>rotate</code>, <code>scale</code> and <code>transform</code>. You can push the drawing state to a state stack with <code>save</code> and later pop it back with <code>restore</code>.

<p>The coordinate grid has its origin in the top left corner. A point (<em>x</em>, <em>y</em>) is <em>x</em> pixels from the left and <em>y</em> pixels from the top. The canvas of every example on this page has a width of 300 and a height of 150.

<p>The language does not support arithmetic expressions. All numerical values must be plain numbers. You can, however, represent repeating decimals with parentheses. For example, <code>0.(3)</code> is interpreted to mean 0.333333... i.e. 1/3.

<p>The rest of this page describes each command in more detail.

<h2><code>strokeRect</code> <em>x y width height</em></h2>
<p>Draws the outline of a rectangle. <em>x</em> and <em>y</em> specify the top-left corner of the rectangle. <em>width</em> and <em>height</em> specify the width and the height of the rectangle.
<pre class="example" id="example-strokeRect">
set lineWidth 10
strokeRect 100 25 100 100
</pre>
<canvas id="canvas-strokeRect" width="300" height="150"></canvas>

<h2><code>fillRect</code> <em>x y width height</em></h2>
<p>Draws a rectangle. <em>x</em> and <em>y</em> specify the top-left corner of the rectangle. <em>width</em> and <em>height</em> specify the width and the height of the rectangle.
<pre class="example" id="example-fillRect">
fillRect 100 25 100 100
</pre>
<canvas id="canvas-fillRect" width="300" height="150"></canvas>

<h2><code>fillText</code> <em>x y text</em></h2>
<p>Draws a character string. <em>x</em> and <em>y</em> specify the position.
<pre class="example" id="example-fillText">
set font 50px sans-serif
fillText 10 90 Hello, world!
</pre>
<canvas id="canvas-fillText" width="300" height="150"></canvas>
<h2><code>strokeText</code> <em>x y text</em></h2>
<p>Draws the outline of a character string. <em>x</em> and <em>y</em> specify the position.
<pre class="example" id="example-strokeText">
set font 50px sans-serif
strokeText 10 90 Hello, world!
</pre>
<canvas id="canvas-strokeText" width="300" height="150"></canvas>

<h2><code>stroke</code> <em>path</em></h2>
<p>Outlines a path. The path can be constructed from primitives <code>M</code>, <code>L</code>, <code>Q</code>, <code>C</code>, <code>A</code>, and <code>Z</code>.

<p><code>M</code> <em>x y</em> moves the pen position to the point (<em>x</em>, <em>y</em>).

<p><code>L</code> <em>x y</em> draws a straight line from the pen position to the point (<em>x</em>, <em>y</em>).
<pre class="example" id="example-L">
set lineWidth 10
stroke M 50 75 L 250 75
</pre>
<canvas id="canvas-L" width="300" height="150"></canvas>

<p><code>Q</code> <em>cpx cpy x y</em> draws a quadratic Bézier curve from the pen position to the point (<em>x</em>, <em>y</em>). The point (<em>cpx</em>, <em>cpy</em>) is the control point.
<pre class="example" id="example-Q">
set lineWidth 10
stroke M 50 75 Q 150 0 250 75
</pre>
<canvas id="canvas-Q" width="300" height="150"></canvas>

<p><code>C</code> <em>cp1x cp1y cp2x cp2y x y</em> draws a cubic Bézier curve from the pen position to the point (<em>x</em>, <em>y</em>). The points (<em>cp1x</em>, <em>cp1y</em>) and (<em>cp2x</em>, <em>cp2y</em>) are the control points.
<pre class="example" id="example-C">
set lineWidth 10
stroke M 50 75 C 150 0 150 150 250 75
</pre>
<canvas id="canvas-C" width="300" height="150"></canvas>

<p><code>A</code> <em>x y r startAngle endAngle</em> draws a circular arc centered at (<em>x</em>, <em>y</em>) and with radius <em>r</em>. The arc starts at <em>startAngle</em> and ends at <em>endAngle</em>, going clockwise. Both angles are measured clockwise from the positive x axis (i.e. from 3 o'clock). The unit of angle is the turn (1 turn equals 360 degrees).
<pre class="example" id="example-A">
set lineWidth 10
stroke A 150 75 50 0 0.75
</pre>
<canvas id="canvas-A" width="300" height="150"></canvas>

<p><code>Z</code> closes the path.
<pre class="example" id="example-Z">
set lineWidth 10
stroke M 150 20 L 225 130 L 75 130 Z
</pre>
<canvas id="canvas-Z" width="300" height="150"></canvas>

<h2><code>fill</code> <em>path</em></h2>
<p>Fills a path. See the <code>stroke</code> command for the path primitives.
<pre class="example" id="example-fill">
set lineWidth 10
fill M 150 20 L 225 130 L 75 130 Z
</pre>
<canvas id="canvas-fill" width="300" height="150"></canvas>

<h2><code>clip</code> <em>path</em></h2>
<p>Further constrains the clipping region to <em>path</em>. See the <code>stroke</code> command for the path primitives.
<pre class="example" id="example-clip">
stroke A 60 75 50 0 1
clip A 60 75 50 0 1
fillText 0 75 This text is clipped by the circular clipping region.
</pre>
<canvas id="canvas-clip" width="300" height="150"></canvas>


<h2><code>translate</code> <em>x y</em></h2>
<p>Changes the transformation matrix to translate by <em>x</em> horizontally and <em>y</em> vertically.
<pre class="example" id="example-translate">
fillRect 50 50 50 50
translate 10 10
set fillStyle #FF0000
fillRect 50 50 50 50
</pre>
<canvas id="canvas-translate" width="300" height="150"></canvas>

<h2><code>rotate</code> <em>angle</em></h2>
<p>Changes the transformation matrix to rotate clockwise by <em>angle</em> turns (1 turn equals 360 degrees).
<pre class="example" id="example-rotate">
fillRect 50 50 50 50
rotate 0.02
set fillStyle #FF0000
fillRect 50 50 50 50
</pre>
<canvas id="canvas-rotate" width="300" height="150"></canvas>

<h2><code>scale</code> <em>x y</em></h2>
<p>Changes the transformation matrix to scale by a factor of <em>x</em> horizontally and <em>y</em> vertically. <em>x</em> and <em>y</em> can be negative.
<pre class="example" id="example-scale">
fillRect 25 25 25 25
scale 2 2
set fillStyle #FF0000
fillRect 25 25 25 25
</pre>
<canvas id="canvas-scale" width="300" height="150"></canvas>

<h2><code>transform</code> <em>m11 m12 m21 m22 dx dy</em></h2>
<p>Changes the transformation matrix to apply the matrix [[<em>m11</em>, <em>m21</em>, <em>dx</em>], [<em>m12</em>, <em>m22</em>, <em>dy</em>], [0, 0, 1]].

<h2><code>set fillStyle</code> <em>value</em></h2>
<p>The <code>fillStyle</code> property determines the color used for filling shapes. The default is <code>black</code>.
<pre class="example" id="example-fillStyle">
set fillStyle #FF0000
fillRect 25 25 50 50
set fillStyle #0000FF
fillRect 50 50 50 50
</pre>
<canvas id="canvas-fillStyle" width="300" height="150"></canvas>

<h2><code>set strokeStyle</code> <em>value</em></h2>
<p>The <code>strokeStyle</code> property determines the color used for stroked segments. The default is <code>black</code>.
<pre class="example" id="example-strokeStyle">
set lineWidth 10
set strokeStyle #FF0000
strokeRect 25 25 50 50
set strokeStyle #0000FF
strokeRect 50 50 50 50
</pre>
<canvas id="canvas-strokeStyle" width="300" height="150"></canvas>

<h2><code>set lineWidth</code> <em>value</em></h2>
<p>The <code>lineWidth</code> property determines the width of stroked segments. The default is 1. Half the stroke is drawn on each side of the segment.
<pre class="example" id="example-lineWidth">
set lineWidth 2
stroke M 50 50 L 250 50
set lineWidth 20
stroke M 50 100 L 250 100
</pre>
<canvas id="canvas-lineWidth" width="300" height="150"></canvas>

<h2><code>set lineCap</code> <em>value</em></h2>
<p>The <code>lineCap</code> property determines how the endpoints of stroked paths are drawn. With <code>round</code>, the stroke ends in a semi-circle. With <code>butt</code>, the stroke ends with the path. With <code>square</code>, the stroke ends in a square. The default is <code>butt</code>.
<pre class="example" id="example-lineCap">
set lineWidth 20
set lineCap round
stroke M 75 25 L 75 125
set lineCap butt
stroke M 150 25 L 150 125
set lineCap square
stroke M 225 25 L 225 125 
</pre>
<canvas id="canvas-lineCap" width="300" height="150"></canvas>

<h2><code>set lineJoin</code> <em>value</em></h2>
<p>The <code>lineJoin</code> property determines how stroked segments are joined together. With <code>round</code>, the outer edges are joined in a circular arc. With <code>bevel</code>, the outer edges are joined with a straight line. With <code>miter</code>, the outer edges are extended until they meet (unless the angle is too sharp). The default is <code>miter</code>.
<pre class="example" id="example-lineJoin">
set lineWidth 20
set lineJoin round
stroke M 160 -10 L 60 75 L 160 160
set lineJoin bevel
stroke M 200 -10 L 100 75 L 200 160
set lineJoin miter
stroke M 240 -10 L 140 75 L 240 160
</pre>
<canvas id="canvas-lineJoin" width="300" height="150"></canvas>

<h2><code>set lineDash</code> <em>value</em></h2>
<p>The <code>lineDash</code> property determines the line dash pattern. The value is the list of distances to alternatively draw a line and a dash.
<pre class="example" id="example-lineDash">
set lineWidth 4
set lineDash 10 10
stroke M 0 50 L 300 50
set lineDash 5 20
stroke M 0 100 L 300 100
</pre>
<canvas id="canvas-lineDash" width="300" height="150"></canvas>

<h2><code>set lineDashOffset</code> <em>value</em></h2>
<p>The <code>lineDashOffset</code> property determines the offset (i.e. the phase) of the line dash pattern.
<pre class="example" id="example-lineDashOffset">
set lineWidth 4
set lineDash 20 20
set lineDashOffset 10
stroke M 0 50 L 300 50
set lineDashOffset 0
stroke M 0 100 L 300 100
</pre>
<canvas id="canvas-lineDashOffset" width="300" height="150"></canvas>

<h2><code>set font</code> <em>value</em></h2>
<p>The <code>font</code> property determines the text style used when drawing text. The syntax is that of the <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font">CSS font property</a>. The default is <code>10px sans-serif</code>.
<pre class="example" id="example-font">
set font italic 30px serif
fillText 0 50 abcde
set font 40px sans-serif
fillText 0 100 abcde
</pre>
<canvas id="canvas-font" width="300" height="150"></canvas>

<h2><code>set textAlign</code> <em>value</em></h2>
<p>The <code>textAlign</code> property determines where the anchor point is horizontally. With <code>left</code>, the anchor point is on the left edge of the text. With <code>right</code>, the anchor point is on the right edge of the text. With <code>center</code>, the anchor point is in the center of the text. With <code>start</code>, the anchor point is at the normal start of the line (on the left for left-to-right locales, on the right for right-to-left locales). With <code>end</code>, the anchor point is at the normal end of the line. The default is <code>start</code>.
<pre class="example" id="example-textAlign">
stroke M 150 0 L 150 150
set font 15px sans-serif
set textAlign left
fillText 150 20 anchor on the left
set textAlign right
fillText 150 50 anchor on the right
set textAlign center
fillText 150 80 anchor in the center
set textAlign start
fillText 150 110 anchor at the start
set textAlign end
fillText 150 140 anchor at the end
</pre>
<canvas id="canvas-textAlign" width="300" height="150"></canvas>

<h2><code>set textBaseline</code> <em>value</em></h2>
<p>The <code>textBaseline</code> property determines the text baseline, i.e. where the anchor point is vertically. With <code>top</code>, the baseline is the top of the em square. With <code>hanging</code>, the baseline is the hanging baseline. With <code>middle</code>, the baseline is the middle of the em square. With <code>alphabetic</code>, the baseline is the normal alphabetic baseline. With <code>ideographic</code>, the baseline is the bottom of the body of the characters. With <code>bottom</code>, the baseline is the bottom of the bounding box. The default value is <code>alphabetic</code>.
<pre class="example" id="example-textBaseline">
stroke M 0 75 L 300 75
set font 15px sans-serif
set textBaseline top
fillText 0 75 fp1
set textBaseline hanging
fillText 50 75 fp2
set textBaseline middle
fillText 100 75 fp3
set textBaseline alphabetic
fillText 150 75 fp4
set textBaseline ideographic
fillText 200 75 fp5
set textBaseline bottom
fillText 250 75 fp6
</pre>
<canvas id="canvas-textBaseline" width="300" height="150"></canvas>

<h2><code>save</code></h2>
<p>Pushes the current drawing attributes, clipping region and transformation matrix onto the state stack. This command is used in conjunction with <code>restore</code>.
<pre class="example" id="example-save">
set font 50px sans-serif
save
set fillStyle #FF0000
fillText 0 50 abc
restore
fillText 0 120 def
</pre>
<canvas id="canvas-save" width="300" height="150"></canvas>

<h2><code>restore</code></h2>
<p>Pops the top state from the state stack and restores its drawing attributes, clipping region and transformation matrix. This command is used in conjunction with <code>save</code>. See the <code>save</code> command for an example.

<script src="bundle.js"></script>
</body>
</html>
